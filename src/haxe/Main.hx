import bugfix.Bugfix;
import debug.Debug;
import game_params.GameParams;
import hf.Hf;
import merlin.Merlin;
import patchman.IPatch;
import patchman.Patchman;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    bugfix: Bugfix,
    debug: Debug,
    gameParams: GameParams,
    merlin: Merlin,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
